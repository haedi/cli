module EDIFACT
    EANCOM_ASSOCIATION_CODES = [
        "EAN001", # GS1 version control number (GS1 Permanent Code) 	
        "EAN002", # GS1 version control number (GS1 Permanent Code) 	
        "EAN003", # GS1 version control number (GS1 Permanent Code) 	
        "EAN004", # GS1 version control number (GS1 Permanent Code) 	
        "EAN005", # GS1 version control number (GS1 Permanent Code) 	
        "EAN006", # GS1 version control number (GS1 Permanent Code) 	
        "EAN007", # GS1 version control number (GS1 Permanent Code) 	
        "EAN008", # GS1 version control number (GS1 Permanent Code) 	
        "EAN009", # GS1 version control number (GS1 Permanent Code) 	
        "EAN010", # GS1 version control number (GS1 Permanent Code) 	
        "EAN011", # GS1 version control number (GS1 Permanent Code) 	
        "GDSN23", # GDSN version 2.3 (GS1 Permanent Code)
    ]

    class Message
        def is_eancom?
            return EANCOM_ASSOCIATION_CODES.include?(@association_assigned_code)
        end
    
        def is_iata?
            return @controlling_agency == "IA"
        end

        def is_edigas?
            return @controlling_agency == "EG"
        end
    end
end